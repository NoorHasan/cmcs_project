<?php $__env->startSection('content'); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-header">
        <h1 class="page-title font_lato">Grage Types </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
                <li class="active">Grage Types</li>
            </ol>
        </div>

    </div>
    <div class="page-content">
        <?php if(session('successMsg')): ?>
            <div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="icon wb-check" aria-hidden="true"></i>
            <?php echo e(session('successMsg')); ?>

            </div>
    <?php endif; ?>
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-md-6">
                        <!-- Example Basic -->
                        <div class="example-wrap">
                            <a class="btn btn-info m-t-15 "href="<?php echo e(route('gragetype.create')); ?>">Add New Type</a>
                            <div class="example table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>type</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $__currentLoopData = $gtypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($type->name); ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-info waves-effect" href="<?php echo e(route('gragetype.edit',$type->id)); ?>">
                                                <i class="material-icons">edit</i>
                                            </a>

                                            <button class="btn btn-danger waves-effect" type="button" onclick="deletetype(<?php echo e($type->id); ?>)">
                                                <i class="material-icons">delete</i>
                                            </button>

                                            <form id="delete-form-<?php echo e($type->id); ?>" action="<?php echo e(route('gragetype.destroy',$type->id)); ?>" method="POST" style="display: none;">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('DELETE'); ?>

                                            </form>

                                        </td>

                                    </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Example Basic -->

                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        function deletetype(id){
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        }

    </script>


<?php $__env->stopSection(); ?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>