<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/pages/invoice.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Service Request Details </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
                <li class="active">Details</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h4>
                            <img width="100" class="margin-right-10" src="https://images.vexels.com/media/users/3/144356/isolated/preview/52fb168f1bd3abf7e97a8e9bfdac331d-speed-car-logo-by-vexels.png">
                            <br><strong>CAR MAINTENANCE CRM SYSTEM </strong></h4>
                        <address>
                            Bhrain,Salmabad
                            <br>
                            <br>
                            <abbr title="Mail"> Garage : </abbr><?php echo e($service->to_garage->username); ?>

                            <br>
                            <abbr title="Mail"> Garage E-mail : </abbr><?php echo e($service->to_garage->email); ?>

                            <br>
                            <abbr title="Phone">Phone : </abbr>&nbsp;&nbsp;<?php echo e($service->to_garage->phone); ?>

                            <br>
                            <abbr> To: <?php echo e($service->entry_by->first_name); ?>&nbsp;<?php echo e($service->entry_by->last_name); ?>

                                <br>

                                <br>
                                <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;<?php echo e($service->entry_by->phone); ?>

                                <br>
                                <span>Appointment suggested Date: <?php echo e($service->sugested_date); ?></span>
                                <br>
                                <span>Appointment  suggested time: <?php echo e($service->sugested_time); ?></span>
                                <h4> Service Title: <?php echo e($service->title); ?></h4>
                        </address>
                    </div>
                    <div class="col-md-3 col-md-offset-6 text-right">
                        <h4>Service Request No</h4>
                        <p>
                            <a class="font-size-20" href="javascript:void(0)">#<?php echo e($service->id); ?></a>

                        </p>


                    </div>
                </div>
                <div>
                    <h4>Problem Description</h4>
                    <p style="font-size: medium"><?php echo e($service->problem_description); ?> </p>
                    <h4>Request status : <?php echo e($service->confirm_status); ?> </h4>
                    <?php if($service->confirm_status == 'Pending'||$service->confirm_status == 'Rejected'): ?>
                    <h4>Reason :</h4>
                    <p style="font-size: medium"><?php echo e($service->reason); ?> </p>
                    <?php endif; ?>
                    <div class="text-right clearfix">
                        <div class="pull-right">

                            <p class="page-invoice-amount">Approximate price range:
                                <span><?php echo e($service->price_range); ?> &nbsp; BD</span>
                            </p>
                        </div>
                    </div>
                    <div class="text-right">
                    <button type="button" class="btn btn-animate btn-animate-side btn-default btn-outline"
                            onclick="javascript:window.print();">
                        <span><i class="icon wb-print" aria-hidden="true"></i> Print</span>
                    </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>
    <br/>
<?php $__env->stopSection(); ?>

 

<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>