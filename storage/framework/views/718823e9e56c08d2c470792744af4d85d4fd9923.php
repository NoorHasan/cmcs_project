<meta charset='utf-8' />

<link href="<?php echo e(asset('fullcalendar/core/main.css')); ?>" rel='stylesheet' />
<link href='fullcalendar/daygrid/main.css' rel='stylesheet' />

<script src='fullcalendar/core/main.js'></script>
<script src='fullcalendar/daygrid/main.js'></script>

<script>



</script>
<?php $__env->startSection('content'); ?>
<style>
canvas{
	width: 95% !important;
	max-width: 100%;
	height: auto !important;
}
</style>
<div class="page-content padding-20 container-fluid">
<!------------------------------ Start Alert message--------------->
<div class="alert alert-primary alert-dismissible alertDismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">×</span>
  </button>
 <?php echo e(trans('app.welcome')); ?>  <?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?> !
</div>
<!-------------------------------- End alert message--------------->
	
<!-------------------------------- start second step graph--------------->
<div class="row">
<div class="col-md-12">
	<div id='calendar'></div>

	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>

<?php
		$userid=Auth::id();
			?>
		$(document).ready(function() {
			// page is now ready, initialize the calendar...
			$('#calendar').fullCalendar({

				// put your options and callbacks here
				events : [
						<?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>{
						title : 'Request for "<?php echo e($s->service_title); ?>"',
						start : '<?php echo e($s->sugested_date); ?>',

						url : '<?php echo e(route('service.show', $s->id)); ?>'},

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						<?php $__currentLoopData = $service_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>{
						<?php if($ss->service->user_id ==Auth::id()||$ss->service->garage_id ==Auth::id()): ?>
						title : 'submit day for "<?php echo e($ss->service->service_title); ?>"',
						start : '<?php echo e($ss->expect_date); ?>',

						url : '<?php echo e(route('servicestatus.show', $ss->id)); ?>'},
					<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>




				]
			})
		});

</script>
  
</div>

</div>

</div>
 <!-------------------------------- end second step graph---------------> 		
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>