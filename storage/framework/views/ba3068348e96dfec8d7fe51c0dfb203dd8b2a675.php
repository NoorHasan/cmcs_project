<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/forms/layouts.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Create Service status </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
                <li class="active">Create Service status</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <!-- Example Basic Form -->

                        <div class="example">
                            <form autocomplete="off" method="POST" action="<?php echo e(route('servicestatus.store')); ?>"  enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="row">

                                    <div class="form-group ">
                                        <label class="control-label" for="service_id">Service Title</label>
                                        <select ng-model="service_id"  class="form-control garage_id " name="service_id" required ng-init="service_id = '<?php echo e(old('service_id')); ?>'">
                                            <option value="">select service </option>
                                            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                            <option value="<?php echo e($service->id); ?>"><?php echo e($service->service_title); ?> </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="expect_date"> Car will be ready on</label>
                                    <input type="date" class="form-control" id="expect_date" name="expect_date"
                                    />
                                </div>


                                    <div class="form-group">
                                        <label class="control-label">Appointment Status</label>
                                        <div>
                                            <div class="radio-custom radio-default radio-inline">
                                                <input type="radio" id="appointment_status" value="Appointment scheduled" name="appointment_status" checked />
                                                <label for="appointment_status_scheduled"  >Appointment scheduled</label>
                                            </div>
                                            <div class="radio-custom radio-default radio-inline">
                                                <input type="radio"  value="Car Dropped"id="appointment" name="appointment_status"  />
                                                <label for="appointment_status">Car Dropped</label>
                                            </div>
                                            <div class="radio-custom radio-default radio-inline">
                                                <input type="radio"  value="Customer Late" id="appointment_status_Customer Late" name="appointment_status"  />
                                                <label for="confirm_status_Customer Late">Customer Late</label>
                                            </div>
                                            <div class="radio-custom radio-default radio-inline">
                                                <input type="radio"  value="Canceled"id="confirm_status_Canceled" name="appointment_status"  />
                                                <label for="confirm_status_Canceled" >Canceled</label>
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group" id="carstatus">
                                    <label class="control-label">Car Status</label>
                                    <div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="In Queue" value="In Queue" name="car_status" checked />
                                            <label for="confirm_status_In_Queue"  >In Queue</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" value="Confirmed"id="need_spear_parts" name="car_status"  />
                                            <label for="need_spear_parts" >Need Spear Parts</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="under_process" value="Under Process" name="car_status"  />
                                            <label for="under_process" >Under Process</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="Ready"  value="Ready" name="confirm_status"  />
                                            <label for="Ready">Ready</label>
                                        </div>
                                    </div>
                                </div>


                                    <div class="form-group">
                                        <label class="control-label" for="price">Service price </label>
                                        <input type="text" class="form-control" id="price" name="price"
                                               placeholder="example : 50  BD" autocomplete="off" />
                                    </div>


                                <div class="form-group">
                                    <a class="btn btn-danger m-t-15 "href="<?php echo e(route('servicestatus')); ?>">Back</a>
                                    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Example Basic Form -->
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php
use Illuminate\Support\Facades\Auth;
$user= Auth::id()
?>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $('#carstatus').hide();
$('#appointment').click(function() {
if($('#radio_button').is(':checked'))
$('#carstatus').show; });
</script>

<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>