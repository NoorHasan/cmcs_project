<?php $__env->startSection('content'); ?>
    <div class="page-header">
        <h1 class="page-title font_lato">Service Status Details </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
                <li class="active">Service Status Details</li>
            </ol>
        </div>
    </div>
    <div class="page-content" style="width:auto; margin:0 auto;">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid " style="width:auto; margin:0 auto">

                <div class="example-example">
                    <h4 class="example-title"><?php echo e($servicestatus->service->garage); ?></h4>
                    <div class="example">
                        <div class="row" >
                            <div class="col-sm-6 col-lg-3">
                                <div class="pricing-list">
                                    <div class="pricing-header">
                                        <div class="pricing-title label label-info" style="font-size: large"><?php echo e($servicestatus->service->service_title); ?></div>
                                        <div class="pricing-price">
                                            <span class="pricing-currency">BD</span>
                                            <span class="pricing-amount"><?php echo e($servicestatus->price); ?></span>

                                        </div>
                                    </div>
                                    <ul class="pricing-features " style="font-size: medium">
                                        <li>
                                            <strong>Appointmet Status</strong>
                                            <?php if($servicestatus->appointment_status=="Appointment scheduled"): ?>
                                                <span class="label label-info"><?php echo e($servicestatus->appointment_status); ?></span>
                                                <?php endif; ?>
                                        <?php if($servicestatus->appointment_status=="Car Dropped"): ?>
                                                <span class="label label-success"><?php echo e($servicestatus->appointment_status); ?></span>
                                            <?php endif; ?>
                                        <?php if($servicestatus->appointment_status=="Customer Late"): ?>
                                                <span class="label label-danger"><?php echo e($servicestatus->appointment_status); ?></span>
                                            <?php endif; ?>
                                        <?php if($servicestatus->appointment_status=="Canceled"): ?>
                                                <span class="label label-dark"><?php echo e($servicestatus->appointment_status); ?></span>
                                            <?php endif; ?>


                                        </li>
                                        <?php if($servicestatus->appointment_status=="Car Dropped"): ?>
                                        <li>
                                            <strong>Car Status</strong>
                                            <?php if($servicestatus->car_status=="Under Process"): ?>
                                                <span class="label label-info"><?php echo e($servicestatus->car_status); ?></span>
                                            <?php endif; ?>
                                            <?php if($servicestatus->car_status=="Ready"): ?>
                                                <span class="label label-success"><?php echo e($servicestatus->car_status); ?></span>
                                            <?php endif; ?>
                                            <?php if($servicestatus->car_status=="Need Spear Parts"): ?>
                                                <span class="label label-danger"><?php echo e($servicestatus->car_status); ?></span>
                                            <?php endif; ?>
                                            <?php if($servicestatus->car_status_status=="In Queue"): ?>
                                                <span class="label label-dark"><?php echo e($servicestatus->car_status); ?></span>
                                            <?php endif; ?>


                                        </li>
                                            <?php endif; ?>

                                    </ul>
                                    <div class="pricing-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Example Pricing Table -->

                    </div>
                </div>
                <!-- End Panel -->
            </div>
            <br/>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>