<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/forms/layouts.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Create Grage Type </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
                <li class="active">Create Grage Type</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <!-- Example Basic Form -->
                        <div class="example-wrap">

                            <div class="example">
                                <form autocomplete="off" method="POST" action="<?php echo e(route('gragetype.store')); ?>"  enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label class="control-label" for="name">Grage Type</label>
                                            <input type="text" class="form-control" id="gragetype" name="name"
                                                   placeholder="Grage Type" autocomplete="off" />
                                        </div>



































                                    </div>
                                    <div class="form-group">
                                        <a class="btn btn-danger m-t-15 "href="<?php echo e(route('gragetype')); ?>">Back</a>
                                        <button type="submit" class="btn btn-primary waves-effect">Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Example Basic Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>