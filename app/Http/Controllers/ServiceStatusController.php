<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ServiceStatus;

class ServiceStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //

        $service_statuses=\App\ServiceStatus::orderBy('expect_date', 'ASC')->get();

        return view('servicestatus.index',compact('service_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $services=Service::where('confirm_status','Confirmed')->where('garage_id',Auth::id())->get();
        return view('servicestatus.create',compact('services'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'service_id' => 'required',
            'appointment_status'=>'required',
            'car_status'=>'required',
            'expect_date'=>'required',
            'price'=>'required',

        ]);
        $servicestatus = new ServiceStatus();
        $servicestatus->service_id=$request->service_id;
        $servicestatus->appointment_status=$request->appointment_status;
        $servicestatus->car_status=$request->car_status;
        $servicestatus->expect_date=$request->expect_date;
        $servicestatus->price=$request->price;

        $servicestatus->save();

        return redirect(route('servicestatus'))->with('successMsg','Status Inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $servicestatus=ServiceStatus::find($id);

        return view('servicestatus.view',compact('servicestatus'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service_status=ServiceStatus::find($id);


        $services=Service::where('confirm_status','Confirmed')->where('garage_id',Auth::id())->get();
        return view('servicestatus.edit',compact('services','service_status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $servicestatus=ServiceStatus::find($id);



        $servicestatus->service_id=$request->service_id;
        $servicestatus->appointment_status=$request->appointment_status;
        $servicestatus->car_status=$request->car_status;
        $servicestatus->expect_date=$request->expect_date;
        $servicestatus->price=$request->price;

        $servicestatus->save();

        return redirect(route('servicestatus'))->with('successMsg','Status Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       ServiceStatus::find($id)->delete();

        return redirect(route('servicestatus'))->with('successMsg','Status Deleted successfully');

        //
    }
}
