<?php

namespace App\Http\Controllers;

use App\GarageTaype;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GrageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // Allow access to authenticated users only.
        $this->middleware('auth');

        // Allow access to users with 'users.manage' permission.
        $this->middleware('permission:app.manage');
    }
    public function index()
    {
        //
        $gtypes=GarageTaype::all();

        return view('gragetype.index',compact('gtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('gragetype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required'
        ]);
        $type=new GarageTaype();
        $type->name=$request->name;
        $type->save();

        return redirect(route('gragetype'))->with('successMsg','Grage Type Inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type=GarageTaype::find($id);

        return view('gragetype.edit',compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $type=GarageTaype::find($id);
        $type->name=$request->name;
        $type->save();
        return redirect(route('gragetype'))->with('successMsg','Garage Type updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //



        GarageTaype::find($id)->delete();

        return redirect(route('gragetype'))->with('successMsg','type deleted successfully');

    }
}
