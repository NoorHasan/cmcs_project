<?php

namespace App\Http\Controllers;

use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\Javascript;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $services=Service::latest()->get();
        return view('service.index',compact( 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $garage_types= User::with('garageType')->whereNotNull('garage_type_id')->groupBy('garage_type_id')->get();
//
        $garages=User::with('garageType')->whereNotNull('garage_type_id')->get();

            //DB::table('users')->whereNotNull('garage_type_id')->get();
        return view('service.create',compact( 'garage_types','garages'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'service_title' => 'required',
            'garage_id'=>'required',
            'problem_description'=>'required',
            'sugested_date'=>'required',
            'sugested_time'=>'required',
            'confirm_status'=>'nullable',
            'reason'=>'nullable',
            'price_range'=>'nullable',
        ]);
        $service=new Service();
        $service->service_title=$request->service_title;
        $service->garage_id=$request->garage_id;
        $service->problem_description=$request->problem_description;
        $service->sugested_date=$request->sugested_date;
        $service->sugested_time=$request->sugested_time;
        $service->confirm_status=$request->confirm_status;
        $service->reason=$request->reason;
        $service->price_range=$request->price_range;
        $service->user_id=Auth::id();
        $service->save();

        return redirect(route('service'))->with('successMsg','Request Inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $service=Service::find($id);
        return view('service.view',compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service=Service::find($id);
        $garage_types= User::with('garageType')->whereNotNull('garage_type_id')->groupBy('garage_type_id')->get();
//
        $garages=User::with('garageType')->whereNotNull('garage_type_id')->get();


        return view('service.edit',compact('service','garage_types','garages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $service=Service::find($id);
        $this->validate($request,[
            'service_title' => 'required',
            'garage_id'=>'required',
            'problem_description'=>'required',
            'sugested_date'=>'required',
            'sugested_time'=>'required',
            'confirm_status'=>'nullable',
            'reason'=>'nullable',
            'price_range'=>'nullable',
        ]);

        $service->service_title=$request->service_title;
        $service->garage_id=$request->garage_id;
        $service->problem_description=$request->problem_description;
        $service->sugested_date=$request->sugested_date;
        $service->sugested_time=$request->sugested_time;
        $service->confirm_status=$request->confirm_status;
        $service->reason=$request->reason;
        $service->price_range=$request->price_range;
        $service->user_id=Auth::id();
        $service->save();

        return redirect(route('service'))->with('successMsg','Request Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Service::find($id)->delete();

        return redirect(route('service'))->with('successMsg','service deleted successfully');
    }

}
