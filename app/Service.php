<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $fillable=['service_title','garage_id','problem_description','sugested_date'
        ,'sugested_time','confirm_status','user_id','reason','price_range'];


    public function entry_by(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function to_garage(){
        return $this->belongsTo(User::class,'garage_id');
    }
    public  function servicestatus(){
        return $this->hasOne(ServiceStatus::class,'service_id');
    }
}
