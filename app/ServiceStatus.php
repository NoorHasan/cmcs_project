<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceStatus extends Model
{
    //

    protected $fillable=['appointment_status','car_status','expect_date','price','service_id'];


    public function service(){
        return $this->belongsTo(Service::class);
    }
}
