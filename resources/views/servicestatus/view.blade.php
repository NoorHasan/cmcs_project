@extends('layouts.template')
@section('content')
    <div class="page-header">
        <h1 class="page-title font_lato">Service Status Details </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Service Status Details</li>
            </ol>
        </div>
    </div>
    <div class="page-content" style="width:auto; margin:0 auto;">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid " style="width:auto; margin:0 auto">

                <div class="example-example">
                    <h4 class="example-title">{{$servicestatus->service->garage}}</h4>
                    <div class="example">
                        <div class="row" >
                            <div class="col-sm-6 col-lg-3">
                                <div class="pricing-list">
                                    <div class="pricing-header">
                                        <div class="pricing-title label label-info" style="font-size: large">{{$servicestatus->service->service_title}}</div>
                                        <div class="pricing-price">
                                            <span class="pricing-currency">BD</span>
                                            <span class="pricing-amount">{{$servicestatus->price}}</span>

                                        </div>
                                    </div>
                                    <ul class="pricing-features " style="font-size: medium">
                                        <li>
                                            <strong>Appointmet Status</strong>
                                            @if($servicestatus->appointment_status=="Appointment scheduled")
                                                <span class="label label-info">{{$servicestatus->appointment_status}}</span>
                                                @endif
                                        @if($servicestatus->appointment_status=="Car Dropped")
                                                <span class="label label-success">{{$servicestatus->appointment_status}}</span>
                                            @endif
                                        @if($servicestatus->appointment_status=="Customer Late")
                                                <span class="label label-danger">{{$servicestatus->appointment_status}}</span>
                                            @endif
                                        @if($servicestatus->appointment_status=="Canceled")
                                                <span class="label label-dark">{{$servicestatus->appointment_status}}</span>
                                            @endif


                                        </li>
                                        @if($servicestatus->appointment_status=="Car Dropped")
                                        <li>
                                            <strong>Car Status</strong>
                                            @if($servicestatus->car_status=="Under Process")
                                                <span class="label label-info">{{$servicestatus->car_status}}</span>
                                            @endif
                                            @if($servicestatus->car_status=="Ready")
                                                <span class="label label-success">{{$servicestatus->car_status}}</span>
                                            @endif
                                            @if($servicestatus->car_status=="Need Spear Parts")
                                                <span class="label label-danger">{{$servicestatus->car_status}}</span>
                                            @endif
                                            @if($servicestatus->car_status_status=="In Queue")
                                                <span class="label label-dark">{{$servicestatus->car_status}}</span>
                                            @endif


                                        </li>
                                            @endif

                                    </ul>
                                    <div class="pricing-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Example Pricing Table -->

                    </div>
                </div>
                <!-- End Panel -->
            </div>
            <br/>
        </div>
    </div>

@stop