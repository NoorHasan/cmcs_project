@extends('layouts.template')
@section('content')
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/layouts.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Create Service status </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Create Service status</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <!-- Example Basic Form -->

                        <div class="example">
                            <form autocomplete="off" method="POST"
                                  action="{{ route('servicestatus.update',$service_status->id) }}"
                                  enctype="multipart/form-data">

                                @csrf
                                <div class="form-group ">
                                    <label class="control-label" for="service_id">Service Title</label>
                                    <select ng-model="service_id" class="form-control garage_id " name="service_id"
                                            required ng-init="service_id = '{{ old('service_id') }}'">
                                        <option value="">select service</option>
                                        @foreach($services as $service)

                                            <option value="{{$service->id}}" {{$service_status->service_id==$service->id?'selected':''}}>{{$service->service_title}} </option>
                                        @endforeach


                                    </select>
                                </div>


                                <div class="form-group">
                                    <label class="control-label" for="expect_date"> Car will be ready on</label>
                                    <input type="date" class="form-control" id="expect_date"
                                           value="{{$service_status->expect_date}}" name="expect_date"
                                    />
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Appointment Status</label>
                                    <div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="appointment_status" value="Appointment scheduled"
                                                   name="appointment_status"
                                                   checked {{ (($service_status->appointment_status == 'Appointment scheduled')?'checked': '')}} />
                                            <label for="appointment_status_scheduled">Appointment scheduled</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="appointment" value="Car Dropped"
                                                   name="appointment_status" {{ (($service_status->appointment_status == 'Car Dropped')?'checked': '')}} />
                                            <label for="appointment_status">Car Dropped</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="appointment_status_Customer Late"
                                                   value="Customer Late"
                                                   name="appointment_status" {{ (($service_status->appointment_status == 'Customer Late')?'checked': '')}} />
                                            <label for="confirm_status_Customer Late">Customer Late</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="confirm_status_Canceled" value="Canceled"
                                                   name="appointment_status" {{ (($service_status->appointment_status == 'Canceled')?'checked': '')}} />
                                            <label for="confirm_status_Canceled">Canceled</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="carstatus">
                                    <label class="control-label">Car Status</label>
                                    <div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="In Queue" name="car_status" value="In Queue"
                                                   {{ (($service_status->confirm_status == 'In Queue')?'checked': '')}}  checked/>
                                            <label for="confirm_status_In_Queue">In Queue</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="need_spear_parts" value="Need Spear Parts"
                                                   name="car_status" {{ (($service_status->confirm_status == 'Need Spear Parts')?'checked': '')}} />
                                            <label for="need_spear_parts">Need Spear Parts</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" id="under_process" value="Under Process"
                                                   name="car_status" {{ (($service_status->confirm_status == 'Under Process')?'checked': '')}} />
                                            <label for="under_process">Under Process</label>
                                        </div>
                                        <div class="radio-custom radio-default radio-inline">
                                            <input type="radio" value="Ready" id="Ready"
                                                   name="car_status" {{ (($service_status->confirm_status == 'Ready')?'checked': '')}} />
                                            <label for="Ready">Ready</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label" for="price">price </label>
                                    <input type="text" class="form-control" id="price" name="price"
                                           placeholder="example : 50 -150 BD" value="{{$service_status->price}}"
                                           autocomplete="off"/>
                                </div>


                                <div class="form-group">
                                    <a class="btn btn-danger m-t-15 " href="{{route('servicestatus')}}">Back</a>
                                    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Example Basic Form -->
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
<?php
use Illuminate\Support\Facades\Auth;
$user = Auth::id()
?>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $('#carstatus').hide();
    $('#appointment').click(function () {
        if ($('#radio_button').is(':checked'))
            $('#carstatus').show;
    });
</script>
