@extends('layouts.template')
@section('content')
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/layouts.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Create Service Request </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Create Service Request</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <!-- Example Basic Form -->

                            <div class="example">
                                <form autocomplete="off" method="POST" action="{{ route('service.store') }}"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label class="control-label" for="service_title">Service Title</label>
                                            <input type="text" class="form-control" id="service_title" name="service_title"
                                                   placeholder="example: tire damage " autocomplete="off" />
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label class="control-label" for="garage_id">Garage</label>
                                            <select ng-model="garage_id"  class="form-control garage_id " name="garage_id" required ng-init="garage_id = '{{ old('garage_id') }}'">
                                                <option value="">select garage </option>
                                                @foreach($garage_types as $garage_type)
                                                <optgroup label="{{$garage_type->garageType->name}}">


                                                @foreach($garages as $garage)
                                                    @if($garage->garage_type_id==$garage_type->garage_type_id)

                                                    <option value="{{$garage->id}}">{{$garage->username}} </option>

                                                    @endif
                                                @endforeach
                                                </optgroup>
                                                    @endforeach


                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label" for="problem_description">Problem Discription</label>
                                        <input type="text" class="form-control" id="problem_description" name="problem_description"
                                               placeholder="example: flat tire" autocomplete="off" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="sugested_date"> Sugested date to drop the car</label>
                                        <input type="date" class="form-control" id="sugested_date" name="sugested_date"
                                               />
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="sugested_time"> Sugested time to drop the car</label>
                                        <input type="time" class="form-control" id="sugested_time" name="sugested_time"
                                        />
                                    </div>
                                    @permission('garage.manage')
                                        <div id="garage_fields">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Request status</label>
                                                                            <div>
                                                                                <div class="radio-custom radio-default radio-inline">
                                                                                    <input type="radio" id="inputBasicMale"  value="Pending"name="confirm_status" checked />
                                                                                    <label for="confirm_status_pending" >Pending</label>
                                                                                </div>
                                                                                <div class="radio-custom radio-default radio-inline">
                                                                                    <input type="radio" id="confirm_status_confirm"  value="Confirmed" name="confirm_status"  />
                                                                                    <label for="confirm_status_confirmed">Confirmed</label>
                                                                                </div>
                                                                                <div class="radio-custom radio-default radio-inline">
                                                                                    <input type="radio" id="confirm_status_rejected" value="Rejected" name="confirm_status"  />
                                                                                    <label for="confirm_status_rejected" >Rejected</label>
                                                                                </div>
                                                                                <div class="radio-custom radio-default radio-inline">
                                                                                    <input type="radio" id="confirm_status_closed" value="Closed"name="confirm_status"  />
                                                                                    <label for="confirm_status_closed" >Closed</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <div class="form-group">
                                        <label class="control-label" for="reason">Holding /Rejected Reason</label>
                                        <input type="text" class="form-control" id="reason" name="reason"
                                               placeholder="example : We will be close at this date for Labor Day" autocomplete="off" />
                                    </div>
                                            <div class="form-group">
                                                <label class="control-label" for="price_range">Approximate price Range</label>
                                                <input type="text" class="form-control" id="price_range" name="price_range"
                                                       placeholder="example : 50 -150 BD" autocomplete="off" />
                                            </div>
                                    </div>
                                    @endpermission
                                    <div class="form-group">
                                        <a class="btn btn-danger m-t-15 "href="{{route('service')}}">Back</a>
                                        <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Example Basic Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<?php
use Illuminate\Support\Facades\Auth;
$user= Auth::id()
?>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

