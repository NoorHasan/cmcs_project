@extends('layouts.template')
@section('content')
    <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/filament-tablesaw/tablesaw.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Service Requests </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Service Requests</li>
            </ol>
        </div>
    </div>
    @if(session('successMsg'))
                <div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                {{session('successMsg')}}
                </div>
        @endif
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <!-- Panel Sortable -->
                <div class="panel">
                    <header class="panel-heading">

                    </header>
                    <div class="panel-body">
                    <div class="example-wrap">
                                                <a class="btn btn-info m-t-15 "href="{{route('service.create')}}">Add New Service</a>
                                                <div class="example table-responsive">
                        <table class="tablesaw table-bordered table-hover"  data-tablesaw-sortable
                               data-tablesaw-sortable-switch>
                            <thead>
                            <tr>
                                <th data-tablesaw-sortable-col data-tablesaw-sortable-numeric>#</th>
                                <th data-tablesaw-sortable-col>Service Title</th>
                                <th id="third" data-tablesaw-sortable-col>Garage</th>

                                <th data-tablesaw-sortable-col>Sugested Date</th>
                                <th data-tablesaw-sortable-col>Sugested Time</th>
                                <th>Confirm Status</th>

                                <th>Costumer</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($services as $key=>$service)
                                @if(Auth::id()==$service->user_id||Auth::id()==$service->garage_id)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td>{{$service->service_title}}</td>
                                    <td>{{$service->to_garage->username}}</td>
                                    <td>{{$service->sugested_date}}</td>
                                    <td>{{$service->sugested_time}}</td>
                                    <td>{{$service->confirm_status}}</td>
                                    <td>{{$service->entry_by->username}}</td>

                                    <td class="text-center">
                                        <a class="btn btn-info waves-effect" href="{{ route('service.edit',$service->id) }}">
                                            <i class="material-icons">edit</i>
                                        </a>
                                             <a class="btn btn-info waves-effect" href="{{ route('service.show',$service->id) }}">
                                              </button>

                                                                                     <form id="delete-form-{{ $service->id }}" action="{{ route('service.destroy',$service->id) }}" method="POST" style="display: none;">
                                                                                         @csrf
                                                                                         @method('DELETE')

                                                                                     </form>
                                                                                                                                                                                     <i class="material-icons">view</i>
                                                                                                                                                                                 </a>
                                        <button class="btn btn-danger waves-effect" type="button" onclick="deleteservice({{ $service->id }})">
                                            <i class="material-icons">delete</i>


                                    </td>
   @endif

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Panel Sortable -->
            </div>

        </div>
    </div>
    <br/>
@stop
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
    function deleteservice(id){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                event.preventDefault();
                document.getElementById('delete-form-'+id).submit();

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }

</script>

