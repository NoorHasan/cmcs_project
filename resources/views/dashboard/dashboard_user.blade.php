@extends('layouts.template')
<meta charset='utf-8' />

<link href="{{asset('fullcalendar/core/main.css')}}" rel='stylesheet' />
<link href='fullcalendar/daygrid/main.css' rel='stylesheet' />

<script src='fullcalendar/core/main.js'></script>
<script src='fullcalendar/daygrid/main.js'></script>

<script>



</script>
@section('content')
<style>
canvas{
	width: 95% !important;
	max-width: 100%;
	height: auto !important;
}
</style>
<div class="page-content padding-20 container-fluid">
<!------------------------------ Start Alert message--------------->
<div class="alert alert-primary alert-dismissible alertDismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">×</span>
  </button>
 {{ trans('app.welcome')}}  {{Auth::user()->first_name}} {{Auth::user()->last_name}} !
</div>
<!-------------------------------- End alert message--------------->
	
<!-------------------------------- start second step graph--------------->
<div class="row">
<div class="col-md-12">
	<div id='calendar'></div>

	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>

<?php
		$userid=Auth::id();
			?>
		$(document).ready(function() {
			// page is now ready, initialize the calendar...
			$('#calendar').fullCalendar({

				// put your options and callbacks here
				events : [
						@foreach($service as $s){
						title : 'Request for "{{ $s->service_title }}"',
						start : '{{ $s->sugested_date }}',

						url : '{{ route('service.show', $s->id) }}'},

					@endforeach

						@foreach($service_status as $ss){
						@if($ss->service->user_id ==Auth::id()||$ss->service->garage_id ==Auth::id())
						title : 'submit day for "{{ $ss->service->service_title }}"',
						start : '{{ $ss->expect_date }}',

						url : '{{ route('servicestatus.show', $ss->id) }}'},
					@endif
					@endforeach




				]
			})
		});

</script>
  
</div>

</div>

</div>
 <!-------------------------------- end second step graph---------------> 		
</div>

@stop
