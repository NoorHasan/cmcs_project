@extends('layouts.template')
@section('content')
@section('content')
    <div class="page-header">
        <h1 class="page-title font_lato">Grage Types </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Grage Types</li>
            </ol>
        </div>

    </div>
    <div class="page-content">
        @if(session('successMsg'))
            <div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="icon wb-check" aria-hidden="true"></i>
            {{session('successMsg')}}
            </div>
    @endif
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-md-6">
                        <!-- Example Basic -->
                        <div class="example-wrap">
                            <a class="btn btn-info m-t-15 "href="{{route('gragetype.create')}}">Add New Type</a>
                            <div class="example table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>type</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($gtypes as $key=>$type)



                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$type->name}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-info waves-effect" href="{{ route('gragetype.edit',$type->id) }}">
                                                <i class="material-icons">edit</i>
                                            </a>

                                            <button class="btn btn-danger waves-effect" type="button" onclick="deletetype({{ $type->id }})">
                                                <i class="material-icons">delete</i>
                                            </button>

                                            <form id="delete-form-{{ $type->id }}" action="{{ route('gragetype.destroy',$type->id) }}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')

                                            </form>

                                        </td>

                                    </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Example Basic -->

                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        function deletetype(id){
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        }

    </script>


@stop
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>