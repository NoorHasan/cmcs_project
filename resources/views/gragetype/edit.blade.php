@extends('layouts.template')
@section('content')
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/layouts.css">
    <div class="page-header">
        <h1 class="page-title font_lato">Create Grage Type </h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
                <li class="active">Create Grage Type</li>
            </ol>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <!-- Example Basic Form -->
                        <div class="example-wrap">

                            <div class="example">

                                <form autocomplete="off" method="POST" action="{{ route('gragetype.update',$type->id) }}"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label class="control-label" for="name">Grage Type</label>
                                            <input type="text" class="form-control" value="{{$type->name}}" id="gragetype" name="name"
                                                   placeholder="Grage Type" autocomplete="off" />
                                        </div>
                                        {{--                                        <div class="form-group col-sm-6">--}}
                                        {{--                                            <label class="control-label" for="inputBasicLastName">Last Name</label>--}}
                                        {{--                                            <input type="text" class="form-control" id="inputBasicLastName" name="inputLastName"--}}
                                        {{--                                                   placeholder="Last Name" autocomplete="off" />--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="form-group">--}}
                                        {{--                                        <label class="control-label">Gender</label>--}}
                                        {{--                                        <div>--}}
                                        {{--                                            <div class="radio-custom radio-default radio-inline">--}}
                                        {{--                                                <input type="radio" id="inputBasicMale" name="inputGender" />--}}
                                        {{--                                                <label for="inputBasicMale">Male</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="radio-custom radio-default radio-inline">--}}
                                        {{--                                                <input type="radio" id="inputBasicFemale" name="inputGender" checked />--}}
                                        {{--                                                <label for="inputBasicFemale">Female</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="form-group">--}}
                                        {{--                                        <label class="control-label" for="inputBasicEmail">Email Address</label>--}}
                                        {{--                                        <input type="email" class="form-control" id="inputBasicEmail" name="inputEmail"--}}
                                        {{--                                               placeholder="Email Address" autocomplete="off" />--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="form-group">--}}
                                        {{--                                        <label class="control-label" for="inputBasicPassword">Password</label>--}}
                                        {{--                                        <input type="password" class="form-control" id="inputBasicPassword" name="inputPassword"--}}
                                        {{--                                               placeholder="Password" autocomplete="off" />--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="form-group">--}}
                                        {{--                                        <div class="checkbox-custom checkbox-default">--}}
                                        {{--                                            <input type="checkbox" id="inputBasicRemember" name="inputCheckbox" checked autocomplete="off"--}}
                                        {{--                                            />--}}
                                        {{--                                            <label for="inputBasicRemember">Remember Me</label>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="form-group">
                                        <a class="btn btn-danger m-t-15 "href="{{route('gragetype')}}">Back</a>
                                        <button type="submit" class="btn btn-primary waves-effect">update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Example Basic Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop